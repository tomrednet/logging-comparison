package net.tomred.logging;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.AfterClass;

public abstract class LoggingTest {

	protected List<Long> thread1;
	protected List<Long> thread5;
	protected List<Long> thread10;
	protected String logger;
	
	public LoggingTest(){
		thread1 = new ArrayList<>(10);
		thread5 = new ArrayList<>(10);
		thread10 = new ArrayList<>(10);
	}

	@AfterClass
	public void stats() {
		System.out.println(System.getProperties().getProperty("java.class.path", ""));
		System.out.println("*******************************************************");
		StringBuilder st = new StringBuilder();
		printHeaders(st);
		printStats(st, thread1, 1);
		printStats(st, thread5, 5);
		printStats(st, thread10, 10);
		System.out.println(st.toString());
	}
	
	private void printHeaders(StringBuilder st) {
		st.append("logger").append("\t")
		.append("Threads").append("\t")
		.append("Run-1").append("\t")
		.append("Run-2").append("\t")
		.append("Run-3").append("\t")
		.append("Run-4").append("\t")
		.append("Run-5").append("\t")
		.append("Run-6").append("\t")
		.append("Run-7").append("\t")
		.append("Run-8").append("\t")
		.append("Run-9").append("\t")
		.append("Run-10").append("\t")
		.append("Average");
	}

	private void printStats(StringBuilder st, List<Long> threads, int num) {
		long avg = 0;
		st.append("\n").append(logger).append("\t").append(num).append("\t");
		for (Long l : threads) {
			avg += l;
			st.append(l).append("\t");
		}
		st.append((avg / 10));
	}
}
