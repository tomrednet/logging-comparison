package net.tomred.logging;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

public class Log4jTest extends LoggingTest {

    private static final Logger log = Logger.getLogger(Log4jTest.class);

    public Log4jTest() {
        logger = "Log4j";
        log.debug("warm up logger");
    }

    @Test(invocationCount = 10)
    public void singleThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line " + i);
        }
        thread1.add(System.currentTimeMillis() - start);
    }

    @Test(invocationCount = 10, threadPoolSize = 5)
    public void multi5ThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line " + i);
        }
        thread5.add(System.currentTimeMillis() - start);
    }

    @Test(invocationCount = 10, threadPoolSize = 10)
    public void multi10ThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line " + i);
        }
        thread10.add(System.currentTimeMillis() - start);
    }
}
