package net.tomred.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;

public class Log4j2Test extends LoggingTest {

    private static final Logger log = LogManager.getLogger(Log4j2Test.class.getName());
    
    public Log4j2Test() {
        logger = "Log4j2";
        log.debug("warm up logger");
    }

    @Test(invocationCount = 10)
    public void singleThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line " + i);
        }
        thread1.add(System.currentTimeMillis() - start);
    }

    @Test(invocationCount = 10, threadPoolSize = 5)
    public void multi5ThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line " + i);
        }
        thread5.add(System.currentTimeMillis() - start);
    }

    @Test(invocationCount = 10, threadPoolSize = 10)
    public void multi10ThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line " + i);
        }
        thread10.add(System.currentTimeMillis() - start);
    }
}
