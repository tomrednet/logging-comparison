package net.tomred.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import com.netflix.blitz4j.LoggingConfiguration;

public class Blitz4jTest extends LoggingTest {

    static {
        LoggingConfiguration.getInstance().configure();
    }

    private static final Logger log = LoggerFactory.getLogger(Blitz4jTest.class);

    public Blitz4jTest() {
        logger = "Blitz4j";
        log.debug("warm up logger");
    }

    @Test(invocationCount = 10)
    public void singleThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line {}", i);
        }
        thread1.add(System.currentTimeMillis() - start);
    }

    @Test(invocationCount = 10, threadPoolSize = 5)
    public void multi5ThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line {}", i);
        }
        thread5.add(System.currentTimeMillis() - start);
    }

    @Test(invocationCount = 10, threadPoolSize = 10)
    public void multi10ThreadTest() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 500000; i++) {
            log.info("simple log line {}", i);
        }
        thread10.add(System.currentTimeMillis() - start);
    }
}
